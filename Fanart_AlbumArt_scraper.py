# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# File:			Fanart_AlbumArt_scraper.py
# Version:		V1.6
# Author:		Kamaradski 2015
# Contributors:	None
#
# Standalone Python cover and cdart scraper for Fanart.tv
#
# This script heavily relies on the Fanart.tv database
# Please contribute to this database, by submitting your own artwork to the site
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



# Your personal Fanart.tv API key for more recent results (https://fanart.tv/get-an-api-key/)
UserAPIkey = 'add-here'

# Set to True to ENABLE logging screen output to debug log-file
LogDebugToFile = True

# Set to True to ENABLE missing artwork log-file
LogMissingArtwork = True

# Set to True to ENABLE downloaded artwork log-file
LogDownloadArtwork = True

# Set to True to ENABLE statistics log-file
LogStatisticFile = True






# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# 					DO NOT EDIT BELOW HERE
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


# -=-=-=-=-=-=-=-=-=- Loading libraries -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
import os
import time
import random
import os.path
import urllib2
import urllib
import json
import socket
from xml.dom.minidom import parse
from os import listdir
from os.path import isfile, join
from mutagen.id3 import ID3
from mutagen.flac import FLAC



# -=-=-=-=-=-=-=-=-=- Pre-set Variables -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

max_attempts = 5
r = 0
rootDir = os.path.dirname(os.path.realpath(__file__))
myDirList = []
StatsTotalFolders = 0
StatsMP3Folders = 0
StatsFLACFolders = 0
StatsTotalCover = 0
StatsTotalCDart = 0
StatsTotalCoverDL = 0
StatsTotalCDartDL = 0
StatsMP3Files = 0
StatsTotalFiles = 0
StatsFLACFiles = 0
MissingCOVERList = []
MissingCDARTList = []
DownloadedCovers = []
DownloadedCDart = []
starttime = time.strftime("%Y-%m-%d %H:%M:%S")
startdate = time.strftime("%Y-%m-%d")



# -=-=-=-=-=-=-=-=-=- Setting file-states -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

# ---------------------   Delete (&re-create) FILE_Missing if exist:
FILE_Missing = (os.path.dirname(os.path.realpath(__file__)) + '/Scraper_Missing.log')
if os.path.isfile(FILE_Missing):
	os.remove(FILE_Missing)

# ---------------------   Delete (&re-create) FILE_Download if exist:
FILE_Download = (os.path.dirname(os.path.realpath(__file__)) + '/Scraper_Download.log')
if os.path.isfile(FILE_Download):
	os.remove(FILE_Download)
	
# ---------------------   Delete (&re-create) Debug log-file if exist:
FILE_Debug = (os.path.dirname(os.path.realpath(__file__)) + '/Scraper_Debug.log')
if os.path.isfile(FILE_Debug):
	os.remove(FILE_Debug)

# ---------------------   Create statistics file, if none is found
FILE_stats = (os.path.dirname(os.path.realpath(__file__)) + '/Scraper_Statistic.log')
if not os.path.isfile(FILE_stats):
	if not LogStatisticFile:
		os.remove(FILE_stats)


	
# -=-=-=-=-=-=-=-=-=- Define Functions -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-	

# ---------------------   Debug log-file writing	
def writeLogToFile (MyTextString):
	FILE_Debugstr = open(FILE_Debug, 'a')
	FILE_Debugstr.write(MyTextString)
	FILE_Debugstr.close()

	
# -=-=-=-=-=-=-=-=-=- Set script state -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

# ---------------------   Print script header to screen
print ''
print ''
print ' -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-'
print '           Python Fanart.tv Album Artwork Scraper by Kamaradski V1.6'
print ' -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-'
print ''
print ''
print ('********** %s SCAN STARTED **********' % startdate)
print ''

# ---------------------   Test UserAPIkey for valid length
if len(UserAPIkey) > 30:
	print ('Using Client API Key: %s' % UserAPIkey)
else:
	print 'Using Client API key: No key detected'

# ---------------------   Create list of folders below the script location
for dirName, subdirList, fileList in os.walk(rootDir):
	myDirList.append(dirName)
	StatsTotalFolders = StatsTotalFolders + 1


# -=-=-=-=-=-=-=-=-=- Start main loop -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

# ---------------------   Loop through each folder
for i in myDirList:
	currentFiles = [ f for f in listdir(i) if isfile(join(i,f)) ]

	# ---------------------   Variables that need to reset each folder loop
	audioactionList = []
	flacactionList = []
	MBalbumID = None
	score = 0
	iscover = False
	iscdart = False

	if LogDebugToFile:  # ---------------------   Debug Logging
		writeLogToFile ('\n')
		writeLogToFile ('\n')
		writeLogToFile ('For Folder: %s \n' % i)

	print '' # ---------------------   on-screen logging
	print ''
	print ('For Album: %s' % i)

	# ---------------------   Check if artwork exist in current folder
	for z in currentFiles:
		StatsTotalFiles = StatsTotalFiles + 1
		if 'cover.jpg' in z:
			score = score + 1
			iscover = True
			StatsTotalCover = StatsTotalCover + 1

		if 'cdart.png' in z:
			score = score + 1
			iscdart = True
			StatsTotalCDart = StatsTotalCDart + 1

		# ---------------------   File-type counter
		if z.endswith('mp3'):
			StatsMP3Files = StatsMP3Files + 1
		if z.endswith('flac'):
			StatsFLACFiles = StatsFLACFiles + 1

	# ---------------------   MP3 statistics & missing artwork list builder
	if any(z for z in currentFiles if z.endswith('mp3')):
		StatsMP3Folders = StatsMP3Folders + 1
		if not iscover:
			MissingCOVERList.append(i)
		if not iscdart:
			MissingCDARTList.append(i)

	# ---------------------   FLAC statistics & missing artwork list builder
	elif any(z for z in currentFiles if z.endswith('flac')):
		StatsFLACFolders = StatsFLACFolders + 1
		if not iscover:
			MissingCOVERList.append(i)
		if not iscdart:
			MissingCDARTList.append(i)

	# ---------------------   If not MP3 or FLAC: Skip processing
	else:
		if LogDebugToFile:
			writeLogToFile ('No supported audio files detected \n')
		print ('No supported audio files detected')

	# ---------------------   Upon missing artwork: make a list with audio-files
	if score < 2:
		for z in currentFiles:
			if z.endswith(('mp3')):
				audioactionList.append(z)
			if z.endswith(('flac')):
				audioactionList.append(z)

		# ---------------------   Select random audio file from list
		if len(audioactionList) > 0:
			randint = random.choice(audioactionList)
			randint = i+'/'+randint

			# ---------------------   Extract audio-tags, get AlbumID & disk number
			if randint.endswith(('mp3')):
				print "MP3 detected"
				curTAGinfo = ID3 (randint)
				MBalbumID = curTAGinfo.get('TXXX:MusicBrainz Album Id')
				MBTPOS = curTAGinfo.get('TPOS')
			if randint.endswith(('flac')):
				print "FLAC detected"
				curTAGinfo = FLAC (randint)
				if curTAGinfo.get('musicbrainz_albumid') is not None:
					MBalbumID = curTAGinfo.get('musicbrainz_albumid')[0]
				if curTAGinfo.get('discnumber') is not None:
					MBTPOS = curTAGinfo.get('discnumber')[0]
			if MBTPOS:
				MBcurrDisk = MBTPOS[0][0]

			# ---------------------   When AlbumID exist: build MusicBrainz API request
			if MBalbumID is not None:
				print ('AlbumID Found: %s' % MBalbumID)
				if MBTPOS:
					print ('Disknumber: %s' % MBcurrDisk)
				time.sleep(1)
				MBAPIreq = 'http://musicbrainz.org/ws/2/release/' + str(MBalbumID) + '?inc=release-groups'
				FetchedXML = None
				skipit = False
				r=0

				# ---------------------   MusicBrainz API Request
				while FetchedXML is None and r<max_attempts:
					try:
						if r==(max_attempts-1):
							print 'No MusicBrainz result....Skipping'
							skipit = True
							break
						req2 = urllib2.Request(MBAPIreq, headers = { "User-Agent" : "Kamaradski_scraper/V1.6" })
						FetchedXML = parse(urllib2.urlopen( req2, timeout=10))
					
					# ---------------------   Error-handling for http error-codes
					except urllib2.HTTPError as e2:
						myerror = e2.code
						print "MusicBrainz API call returned HTTP error: ",myerror
						if LogDebugToFile:
							writeLogToFile ('MusicBrainz API call returned HTTP error: %s \n' % myerror)
						r=r+1
						print "Re-trying MusicBrainz API-call, attempt -- ",r
						if LogDebugToFile:
							writeLogToFile ('Re-trying MusicBrainz API-call, attempt --  %s \n' % r)
						time.sleep(10)
						pass

					# ---------------------   Error-handling for URL errors
					except urllib2.URLError as e2:
						myerror = e2.args
						print "MusicBrainz API call returned URL error: ",myerror
						if LogDebugToFile:
							writeLogToFile ('MusicBrainz API call returned URL error: %s \n' % myerror)
						r=r+1
						print "Re-trying MusicBrainz API-call, attempt -- ",r
						if LogDebugToFile:
							writeLogToFile ('Re-trying MusicBrainz API-call, attempt --  %s \n' % r)
						time.sleep(10)
						pass

					# ---------------------   Error-handling for socket errors
					except socket.timeout, e2:
						print "MusicBrainz API call returned socket error: ",e2
						if LogDebugToFile:
							writeLogToFile ('MusicBrainz API call returned socket error: %s \n' % e2)
						r=r+1
						print "Re-trying MusicBrainz API-call, attempt -- ",r
						if LogDebugToFile:
							writeLogToFile ('Re-trying MusicBrainz API-call, attempt --  %s \n' % r)
						time.sleep(10)
						pass

				# ---------------------   Process MusicBrainz API result
				if not skipit:
					relGroupElem = FetchedXML.getElementsByTagName('release-group')[0]
					MBrelGroupID = relGroupElem.getAttribute('id')
					print ('MB Release-group: %s' %MBrelGroupID)

					# ---------------------   Build Fanart.tv API call
					if len(UserAPIkey) > 30:
						fanartTVreq = 'http://webservice.fanart.tv/v3/music/albums/' + MBrelGroupID + '?api_key=6f1f3b034c7b179b824d9641425cbd24' + '&client_key=' + UserAPIkey
					else:
						fanartTVreq = 'http://webservice.fanart.tv/v3/music/albums/' + MBrelGroupID + '?api_key=6f1f3b034c7b179b824d9641425cbd24'
					req1 = urllib2.Request(fanartTVreq, headers = { "User-Agent" : "Kamaradski_scraper/V1.6" })
					fanartTVresp = None
					e = None
					myerror = None
					skipcover = False
					skipcdart = False
					r=0

					# ---------------------   Fanart.tv API request
					while fanartTVresp is None and r<max_attempts:
						try:
							if r==(max_attempts-1):
								print 'Fanart.tv API error.... Skipping'
								skipit = True
								break
							fanartTVresp = urllib2.urlopen(req1, timeout=10).read()							

						# ---------------------   Error-handling
						except urllib2.HTTPError as e:
							myerror = e.code
							if e.code == 404:
								if LogDebugToFile:
									writeLogToFile ('No results in Fanart.tv database for this AlbumID. Error-code: %s \n' % myerror)
								print "No results in Fanart.tv database for this AlbumID. Error-code: ",myerror
								break
							if LogDebugToFile:
								writeLogToFile ('Fanart.tv API call returned HTTP error: %s \n' % myerror)
							print "Fanart.tv API call returned HTTP error: ",myerror
							r=r+1
							print "Re-trying Fanart API-call, attempt -- ",r
							time.sleep(15)
							pass

						# ---------------------   Error-handling for URL errors
						except urllib2.URLError as e:
							myerror = e.args
							print "Fanart.tv API call returned URL error: ",myerror
							if LogDebugToFile:
								writeLogToFile ('Fanart.tv API call returned URL error: %s \n' % myerror)
							r=r+1
							print "Re-trying Fanart.tv API-call, attempt -- ",r
							if LogDebugToFile:
								writeLogToFile ('Re-trying Fanart.tv API-call, attempt --  %s \n' % r)
							time.sleep(10)
							pass
						
						# ---------------------   Error-handling for socket errors
						except socket.timeout, e:
							print "Fanart.tv API call returned socket error: ",e
							if LogDebugToFile:
								writeLogToFile ('Fanart.tv API call returned socket error: %s \n' % e)
							r=r+1
							print "Re-trying Fanart.tv API-call, attempt -- ",r
							if LogDebugToFile:
								writeLogToFile ('Re-trying Fanart.tv API-call, attempt --  %s \n' % r)
							time.sleep(10)
							pass
							
					# ---------------------   Process Fanart.tv result
					if myerror != 404:
						fanartTVdata = json.loads(fanartTVresp)						
						artlist = []
						for r in fanartTVdata["albums"][MBrelGroupID]:
							artlist.append(r)
						if not 'albumcover' in artlist:
							skipcover = True
						if not 'cdart' in artlist:
							skipcdart = True

						# ---------------------   Select Cover Downloaded URL & Download File
						if not iscover:
							if not skipcover:
								fanartcoverurl = fanartTVdata["albums"][MBrelGroupID]["albumcover"][0]["url"]
								dload = urllib.URLopener()
								dload.retrieve(fanartcoverurl, (i + "/cover.jpg"))
								StatsTotalCoverDL = StatsTotalCoverDL + 1
								if LogDebugToFile:
									writeLogToFile ('Downloaded cover: %s \n' % fanartcoverurl)
								if i in MissingCOVERList: 
									MissingCOVERList.remove(i)
								DownloadedCovers.append(i)
								print ('Downloaded cover: %s' % fanartcoverurl)

							else:
								if LogDebugToFile:
									writeLogToFile ('No albumcover in the Fanart.tv database \n')
								print 'No albumcover in the Fanart.tv database'

						else:
							if LogDebugToFile:
								writeLogToFile ('cover.jpg exist... skipping \n')
							print 'cover.jpg exist... skipping'

						# ---------------------   Select CDART Downloaded URL & Download File
						if not iscdart:
							if not skipcdart:
								fanartcdarturllst = []
								comparecheck = []

								# ---------------------   If we know about a specific disk number
								if MBcurrDisk >0:
									fanartcdarturliter = fanartTVdata["albums"][MBrelGroupID]["cdart"]
									for a in fanartcdarturliter:
										comparedisk = a["disc"]
										if comparedisk == MBcurrDisk:
											fanartcdarturllst.append(a["url"])
											comparecheck.append(a["disc"])

									# ---------------------   Download cdart for appropriate disk number
									if MBcurrDisk in comparecheck:
										dload = urllib.URLopener()
										dload.retrieve(fanartcdarturllst[0], (i + "/cdart.png"))
										StatsTotalCDartDL = StatsTotalCDartDL + 1
										
										if LogDebugToFile:
											writeLogToFile ('Downloaded cdart: %s \n' % fanartcdarturllst[0])
										
										if i in MissingCDARTList: MissingCDARTList.remove(i)
										DownloadedCDart.append(i)
										print ('Downloaded cdart: %s' % fanartcdarturllst[0])

									# ---------------------   Download first result instead
									else:
										dlthisinstead = fanartTVdata["albums"][MBrelGroupID]["cdart"][0]["url"]
										dload = urllib.URLopener()
										dload.retrieve(dlthisinstead, (i + "/cdart.png"))
										StatsTotalCDartDL = StatsTotalCDartDL + 1
										if LogDebugToFile:
											writeLogToFile ('Downloaded alternative cdart: %s \n' % dlthisinstead)
										if i in MissingCDARTList: 
											MissingCDARTList.remove(i)
										DownloadedCDart.append(i)
										print ('Downloaded alternative cdart: %s' % dlthisinstead)

								# ---------------------   Download first result instead
								else:
									fanartcdarturl = fanartTVdata["albums"][MBrelGroupID]["cdart"][0]["url"]
									dload = urllib.URLopener()
									dload.retrieve(fanartcdarturl, (i + "/cover.jpg"))
									
									if LogDebugToFile:
										writeLogToFile ('CDart: %s \n' % fanartcdarturl)
									print ('CDart: %s' % fanartcdarturl)

							else:
								if LogDebugToFile:
									writeLogToFile ('No cdart in the Fanart.tv database \n')
								print 'No cdart in the Fanart.tv Database'

						else:
							if LogDebugToFile:
								writeLogToFile ('cdart.png exist... skipping \n')
							print 'cdart.png exist... skipping'

					else:
						if LogDebugToFile:
							writeLogToFile ('No information found in Fanart.tv database...\n')
						print 'No information found in Fanart.tv database...'

				else:
					if LogDebugToFile:
						writeLogToFile ('Fanart.tv API error for: %s \n' % MBAPIreq)
					print ('Fanart.tv API error for: %s' % MBAPIreq)

			else:
				if LogDebugToFile:
					writeLogToFile ('Musicbrainz AlbumID tag not found or empty: \n')
				print 'Musicbrainz AlbumID tag not found or empty:'

	else:
		if LogDebugToFile:
			writeLogToFile ('Local fanart detected, skipping further process. \n')
		print ('Local fanart detected, skipping further process.')		


# -=-=-=-=-=-=-=-=-=- Write Missing Artwork to file -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
if LogMissingArtwork:
	FILE_Missingstr = open(FILE_Missing, 'a')
	FILE_Missingstr.write('********** %s Missing COVER Artwork ********** \n' % startdate)

	for t in MissingCOVERList:
		FILE_Missingstr.write('%s \n' % t)

	FILE_Missingstr.write('\n')
	FILE_Missingstr.write('\n')
	FILE_Missingstr.write('\n')
	FILE_Missingstr.write('********** %s Missing CDART Artwork ********** \n' % startdate)

	for t in MissingCDARTList:
		FILE_Missingstr.write('%s \n' % t)


# -=-=-=-=-=-=-=-=-=- Write Download Artwork to file -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
if LogDownloadArtwork:
	FILE_Downloadstr = open(FILE_Download, 'a')
	FILE_Downloadstr.write('********** %s Downloaded COVER Artwork ********** \n' % startdate)

	for t in DownloadedCovers:
		FILE_Downloadstr.write('%s \n' % t)

	FILE_Downloadstr.write('\n')
	FILE_Downloadstr.write('\n')
	FILE_Downloadstr.write('\n')
	FILE_Downloadstr.write('********** %s Downloaded CDART Artwork ********** \n' % startdate)	

	for t in DownloadedCDart:
		FILE_Downloadstr.write('%s \n' % t)
	FILE_Downloadstr.close()


# -=-=-=-=-=-=-=-=-=- Write statistics to file -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
if LogStatisticFile:
	stoptime = time.strftime("%Y-%m-%d %H:%M:%S")
	StatsTotalArtFiles = StatsTotalCover + StatsTotalCDart

	
	# ---------------------   Catch any divide by zero errors
	try:
		StatsMP3Foldersperc = 100.00*StatsMP3Folders/StatsTotalFolders
	except:
		StatsMP3Foldersperc = 0
	
	try:
		StatsFLACFoldersperc = 100.00*StatsFLACFolders/StatsTotalFolders
	except:
		StatsFLACFoldersperc = 0
	
	try:
		StatsTotalCoverperc = 100.00*StatsTotalCover/StatsMP3Folders
	except:
		StatsTotalCoverperc = 0

	try:
		StatsTotalCDartperc = 100.00*StatsTotalCDart/StatsMP3Folders
	except:
		StatsTotalCDartperc = 0

	try:
		StatsMP3Filesperc = 100.00*StatsMP3Files/(StatsTotalFiles-StatsTotalArtFiles)
	except:
		StatsMP3Filesperc = 0

	try:
		StatsFLACFilesperc = 100.00*StatsFLACFiles/(StatsTotalFiles-StatsTotalArtFiles)
	except:
		StatsFLACFilesperc = 0
		
		
	# ---------------------   Actual file-writing
	FILE_statsstr = open(FILE_stats, 'a')
	FILE_statsstr.write('\n')
	FILE_statsstr.write('\n')
	FILE_statsstr.write('********** %s SCAN Summery ********** \n' % startdate)
	FILE_statsstr.write('START: %s \n' % starttime)
	FILE_statsstr.write('STOP: %s \n' % stoptime)
	FILE_statsstr.write('\n')
	FILE_statsstr.write('Total Folders Detected: %d \n' % StatsTotalFolders)
	FILE_statsstr.write('Total Folders containing MP3: %d (%d%%) \n' % (StatsMP3Folders, StatsMP3Foldersperc))
	FILE_statsstr.write('Total Folders containing FLAC: %d (%d%%) \n' % (StatsFLACFolders, StatsFLACFoldersperc))
	FILE_statsstr.write('\n')
	FILE_statsstr.write('Existing local cover.jpg detected: %d (%d%%) \n' % (StatsTotalCover, StatsTotalCoverperc)) 
	FILE_statsstr.write('Existing local cdart.png detected: %d (%d%%) \n' % (StatsTotalCDart, StatsTotalCDartperc)) 
	FILE_statsstr.write('Total local artwork: %d \n' % StatsTotalArtFiles)
	FILE_statsstr.write('\n')
	FILE_statsstr.write('Total audio files Detected: %d \n' % (StatsTotalFiles-StatsTotalArtFiles))
	FILE_statsstr.write('Total MP3 files Detected: %d (%d%%) \n' % (StatsMP3Files, StatsMP3Filesperc)) 
	FILE_statsstr.write('Total FLAC files Detected: %d (%d%%) \n' % (StatsFLACFiles, StatsFLACFilesperc)) 
	FILE_statsstr.write('\n')
	FILE_statsstr.write('NEW cover.jpg downloaded: %d \n' % StatsTotalCoverDL) 
	FILE_statsstr.write('NEW cdart.png downloaded: %d \n' % StatsTotalCDartDL)
	FILE_statsstr.close()


# -=-=-=-=-=-=-=-=-=- Print statistics to screen -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
print ''
print ''
print ('********** %s SCAN Summery **********' % startdate)
print ('START: %s' % starttime)
print ('STOP: %s' % stoptime)
print ''
print ('Total Folders Detected: %d' % StatsTotalFolders)
print ('Total Folders containing MP3: %d (%d%%)' % (StatsMP3Folders, StatsMP3Foldersperc))
print ('Total Folders containing FLAC: %d (%d%%)' % (StatsFLACFolders, StatsFLACFoldersperc))
print ''
print ('Existing local cover.jpg detected: %d (%d%%)' % (StatsTotalCover, StatsTotalCoverperc)) 
print ('Existing local cdart.png detected: %d (%d%%)' % (StatsTotalCDart, StatsTotalCDartperc)) 
print ('Total local artwork: %d ' % StatsTotalArtFiles)
print ''
print ('Total audio files Detected: %d' % (StatsTotalFiles-StatsTotalArtFiles))
print ('Total MP3 files Detected: %d (%d%%)' % (StatsMP3Files, StatsMP3Filesperc)) 
print ('Total FLAC files Detected: %d (%d%%)' % (StatsFLACFiles, StatsFLACFilesperc)) 
print ''
print ('NEW cover.jpg downloaded: %d' % StatsTotalCoverDL) 
print ('NEW cdart.png downloaded: %d' % StatsTotalCDartDL)

# EOF
